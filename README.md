## Requirements

- [vim-plug](https://github.com/junegunn/vim-plug)
- [powerline-font](https://github.com/powerline/fonts)


## Installation

```bash
cd vimrc
cp vimrc ~/.vimrc
vim ~/.vimrc
```

If you use neo-vim, you can make a hard link of .vimrc
```bash
cd 
mkdir .config/nvim
ln .vimrc .config/nvim/init.vim
```

enter flowing command in the vim to install plugins
	
```bash
:PlugInstall

```
Now restart vim.
	
## shortcuts

short-cut | description       | cmd
----------|-------------------|----
<Ctrl+s>  | save file         | :w
<Ctrl+q>  | close tab(buffer) | :bd
<Alt+l>   | previous tab      | :bp
<Alt+h>   | next tab          | :bn
<Ctrl+/>  | toggle tree view  | :NERDTreeToggle
<Ctrl+p>  | open fuzzy finder (like atom editor) | 
<Ctrl+k>  | open fuzzy finder (like qtcreator) | 


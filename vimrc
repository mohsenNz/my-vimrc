"----------------------------------------------------------------------
"-                              Plugins                               -
"----------------------------------------------------------------------

" Plugins will be downloaded under the specified directory.
call plug#begin('~/.vim/plugged')

" Declare the list of plugins.
Plug 'tpope/vim-sensible'
Plug 'junegunn/seoul256.vim'
Plug 'https://github.com/eparreno/vim-l9.git'
Plug 'https://github.com/vim-scripts/AutoComplPop.git'
Plug 'https://github.com/vim-airline/vim-airline.git'
Plug 'https://github.com/vim-airline/vim-airline-themes.git'
Plug 'preservim/nerdcommenter'
Plug 'preservim/nerdtree'
"Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
"Plug 'junegunn/fzf.vim'
Plug 'https://github.com/ctrlpvim/ctrlp.vim.git'
"Plug 'https://github.com/jeetsukumaran/vim-buffergator.git'
Plug 'jiangmiao/auto-pairs'
Plug 'octol/vim-cpp-enhanced-highlight'                       " Cpp highlighting
Plug 'https://github.com/airblade/vim-gitgutter.git'
"Plug 'https://github.com/tpope/vim-fugitive.git'
"Plug 'https://github.com/mileszs/ack.vim.git'
"Plug 'https://github.com/tpope/vim-surround.git'
"Plug 'https://github.com/terryma/vim-multiple-cursors.git'
Plug 'https://github.com/dense-analysis/ale.git'              " Chack syntax
"Plug 'https://github.com/peterhoeg/vim-qml.git'               " Qml syntax highlighting
Plug 'https://github.com/fedorenchik/qt-support.vim.git'      " Qt support for vim
"Plug 'https://github.com/fedorenchik/qt-support.vim.git'
"Plug 'https://github.com/sickill/vim-monokai.git'             " Colorscheme
Plug 'https://gitlab.gnome.org/mohsenNz/vim-monokai-mnz.git'  " Colorscheme
Plug 'https://github.com/tomasr/molokai.git'                  " Colorscheme
Plug 'joshdick/onedark.vim'                                   " Colorscheme onedark
Plug 'https://github.com/milkypostman/vim-togglelist.git'     " toggle qquickfix-window and list-window
Plug 'https://github.com/godlygeek/tabular.git'               " Tabular, aligning text 
Plug 'cespare/vim-toml'                                       " TOML systax highlight
" List ends here. Plugins become visible to Vim after this call.
call plug#end()


"---------------------------------------------------------------------
"-                          Basic Config                             -
"---------------------------------------------------------------------
"
:set number " Display line numbers on the left side
:set ls=2 " This makes Vim show a status line even when only one window is shown
:filetype plugin on " This line enables loading the plugin files for specific file types
:set tabstop=4 " Set tabstop to tell vim how many columns a tab counts for. Linux kernel code expects each tab to be eight columns wide.
:set shiftwidth=4 " Set shiftwidth to control how many columns text is indented with the reindent operations (<< and >>) and automatic C-style indentation. 
:set expandtab " When expandtab is set, hitting Tab in insert mode will produce the appropriate number of spaces.
:set softtabstop=4 " Set softtabstop to control how many columns vim uses when you hit Tab in insert mode. If softtabstop is less than tabstop and expandtab is not set, vim will use a combination of tabs and spaces to make up the desired spacing. If softtabstop equals tabstop and expandtab is not set, vim will always use tabs. When expandtab is set, vim will always use the appropriate number of spaces.
":setlocal foldmethod=syntax " Set folding method
:setlocal foldmethod=indent " Set folding method
:set t_Co=256 " makes Vim use 256 colors
":set nowrap " Don't Wrap lines!
:colorscheme monokai "Set colorScheme
:set nocp " This changes the values of a LOT of options, enabling features which are not Vi compatible but really really nice
:set clipboard=unnamed
:set clipboard=unnamedplus
:set autoindent " Automatic indentation
:set cindent " This turns on C style indentation
:set si " Smart indent
:syntax enable " syntax highlighting
:set showmatch " Show matching brackets
:set hlsearch " Highlight in search
:set ignorecase " Ignore case in search
:set smartcase " Ingore ignorecase if search-pattern has a upper-case word
:set noswapfile " Avoid swap files
:set mouse=a " Mouse Integration
:set hidden " Any buffer can be hidden (keeping its changes) without first writing the buffer to a file on switching buffers. 
:set cursorline " Highlight current line
":hi CursorLine cterm=None ctermbg=234 guibg=#000000
":hi Comment cterm=italic gui=italic " Make comments italic
:set colorcolumn=110 " Add line to show maximum line length
":highlight ColorColumn ctermbg=236
:let mapleader = ","


"---------------------------------------------------------------------
"-                          Plugin Section                           -
"---------------------------------------------------------------------

" - - - - - - - - - - - NERDCommenter plugin - - - - - - - - - - - - -  
" NERDCommenter toggle comment lines with CTRL-/
:map <c-_> <leader>c<space>

" - - - - - - - - - - - Fuzzy-Find plugin - - - - - - - - - - - - - --
" Fuzzy-Find with CTRL-k (like QtCreator)
":map <c-k> :FZF<space>

" - - -  - - - - - - - airline plugin setting - - - - - - - - - - - --
:let g:airline_theme='wombat' " set airline plugin theme
:let g:airline#extensions#tabline#enabled = 1 " showing tabs 
:let g:airline_powerline_fonts = 1

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif
"let g:airline_left_sep = ''
"let g:airline_left_sep = '▶'
"let g:airline_right_sep = ''
"let g:airline_right_sep = '◀'

" - - - - - - - - - - NERDTree plugin setting - - - - - - - - - - - - -
"toggle showing NERDTree with Ctrl+\
:map <c-\> :NERDTreeToggle<CR> 

"open a NERDTree automatically when vim starts up if no files were specified
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" close vim if the only window left open is a NERDTree
"autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" - - - C/C++ highlighting - - - 
:let g:cpp_class_scope_highlight = 1
:let g:cpp_member_variable_highlight = 1
:let g:cpp_class_decl_highlight = 1
:let g:cpp_experimental_template_highlight = 1
:let g:cpp_concepts_highlight = 1

" - - - - - - - - - - - CtrlP plugin setting - - - - - - - - - - - - -
:nmap <c-k> :CtrlP<CR>
"nmap <leader>bm :CtrlPMixed<cr>
"nmap <leader>bs :CtrlPMRU<cr><Paste>

" - - - Togglelist - - - 
":nmap <script> <silent> <leader>l :call ToggleLocationList()<CR>
:nmap <script> <silent> <A-3> :call ToggleQuickfixList()<CR>



"---------------------------------------------------------------------
"                            Other Configs                           -
"---------------------------------------------------------------------
 
" CTRL-s to save file
:nmap <c-s> :w<CR>

" CTRL-q to close buffer
:function CloseBuffer()
: if g:NERDTree.IsOpen()
:   NERDTreeToggle
:   bd 
:   NERDTreeToggle
:   wincmd w
: else
:   bd
:   wincmd w
: endif
:endfunction
:nmap <c-q> :call CloseBuffer()<CR>

" \-d to exit from file
:nmap <Leader>d :bd<CR>

" \-n, Ctrl-2 next buffer
:nmap <Leader>n :bn<CR>
:nmap <A-k> :bn<CR>
:nmap <A-l> :bn<CR>
":nmap <A-2> :bn<CR>

" \-p, Ctrl-1 proviuos buffer

:nmap <Leader>p :bp<CR>
:nmap <A-j> :bp<CR>
:nmap <A-h> :bp<CR>
":nmap <A-1> :bp<CR>

:map <F8> :setlocal spell! spelllang=en_us<CR> " check spelling with F8

:nmap <F12> :setlocal foldmethod=indent<CR>

" Find in All Files, <F4> Find text under cursor
:map <F4> :execute "vimgrep /" . expand("<cword>") . "/j **" <Bar> cw<CR>
:command -nargs=1 F execute "vimgrep /" . <q-args> . "/j **" | copen



